package Crawl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.dog.dao.impl.CrawlDaoImpl;
import com.dog.domain.Crawl;

public class GetThread3 extends Thread {
	public Boolean bool=true;
    private final CloseableHttpClient httpClient;
    private final HttpContext context;
    private final HttpGet httpget;
    public static int i=0;

    public GetThread3(CloseableHttpClient httpClient, HttpGet httpget) {
        this.httpClient = httpClient;
        this.context = HttpClientContext.create();
        this.httpget = httpget;
    }

    @Override
    public void run() {
        try {
            CloseableHttpResponse response = httpClient.execute(
                    httpget, context);
            
            try {
                HttpEntity entity = response.getEntity();
//                Document doc=Jsoup.parse(entity.toString());
                Document doc=Jsoup.parse(EntityUtils.toString(entity,"utf8"));
                String title = doc.select(".contentstyle124904").text(); 
                
//                System.out.println("现在正在请求 ：   "+httpget+"  \n "+title);
//                System.out.println(i+"-----------------");
//                System.out.println(title);
//                System.out.println("----------------");
                if(title.equals("")){
                	bool=false;
    
                }
                Crawl crawl=new Crawl(httpget.getURI().toString(),doc.title().toString(),title);
                CrawlDaoImpl test=new CrawlDaoImpl();
                try {
                	if(bool){
					test.add(crawl);
					System.out.println(httpget.toString()+"添加成功");
                	}
                	
                	else{
                		System.out.println("添加失败");
                	}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println(httpget.toString()+"添加失败");
				}
                
            } finally {
            	
                response.close();
                
                
            }
        } catch (ClientProtocolException ex) {
            
        } catch (IOException ex) {
        }
    }

}

