package Crawl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GetThread1 extends Thread {

    private final CloseableHttpClient httpClient;
    private final HttpContext context;
    private final HttpGet httpget;
    String url="http://cec.jmu.edu.cn/";
    ArrayList lis=new ArrayList();
    public GetThread1(CloseableHttpClient httpClient, HttpGet httpget) {
        this.httpClient = httpClient;
        this.context = HttpClientContext.create();
        this.httpget = httpget;
    }

	public ArrayList getLis() {
		return lis;
	}

	public void setLis(ArrayList lis) {
		this.lis = lis;
	}


	@Override
    public void run() {
        try {
            CloseableHttpResponse response = httpClient.execute(httpget, context);
            try {
                HttpEntity entity = response.getEntity();
                Document doc=Jsoup.parse(EntityUtils.toString(entity,"utf8"));
                Elements links=doc.select(".c124907");    
                for (Element link : links) {  
                	
//                    System.out.println(url +link.attr("href"));
               
                    lis.add(link.attr("href"));
                    
                }  
                  
            } finally {
                response.close();
                 
            }
        } catch (ClientProtocolException ex) {
        } catch (IOException ex) {
        }
    }

}
 

