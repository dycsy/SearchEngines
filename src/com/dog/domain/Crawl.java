package com.dog.domain;

public class Crawl {
    private int id;
    private String url;
    private String abs;
    private String description;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getAbs() {
        return abs;
    }
    public void setAbs(String Abs) {
        this.abs = Abs;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Crawl(int id, String url, String abs, String description) {
        super();
        this.id = id;
        this.url = url;
        this.abs = abs;
        this.description = description;
    }
    public Crawl(String url, String abs, String description) {
        super();
        this.url = url;
        this.abs = abs;
        this.description = description;
    }
    public Crawl() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Override
    public String toString() {
        return "Person [id=" + id + ", url=" + url + ", abs=" + abs
                + ", description=" + description + "]";
    }
    
    
}