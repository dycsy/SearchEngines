package com.dog.lucene.first;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.junit.Test;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.dog.dao.CrawlDao;
import com.dog.dao.impl.CrawlDaoImpl;
import com.dog.domain.Crawl;






public class IndexManager {

	@Test
	public void createIndex() throws Exception {
		// 采集数据
		CrawlDao dao = new CrawlDaoImpl();
		List<Crawl> list = dao.findAll();

		// 将采集到的数据封装到Document对象中
		List<Document> docList = new ArrayList();
		Document document;
		for (Crawl crawl : list) {
			document = new Document();
			// store:如果是yes，则说明存储到文档域中

			Field id = new IntField("id", crawl.getId(), Store.YES);

			Field url = new StoredField("url", crawl.getUrl());
			

			Field abs = new StoredField("abs", crawl.getAbs());
			Field description = new TextField("description",
					crawl.getDescription(), Store.YES);


			document.add(id);
			document.add(url);
			document.add(abs);
			document.add(description);

			docList.add(document);
		}
		// 创建分词器，标准分词器
		// Analyzer analyzer = new StandardAnalyzer();
		// 使用ikanalyzer
		Analyzer analyzer = new IKAnalyzer();

		// 创建IndexWriter
		IndexWriterConfig cfg = new IndexWriterConfig(Version.LUCENE_4_10_3,
				analyzer);
		// 指定索引库的地址
		File indexFile = new File("C:\\test1\\aaa\\");
		Directory directory = FSDirectory.open(indexFile);
		IndexWriter writer = new IndexWriter(directory, cfg);

		// 通过IndexWriter对象将Document写入到索引库中
		for (Document doc : docList) {
			writer.addDocument(doc);
		}

		// 关闭writer
		writer.close();
	}

}
