package com.dog.dao;

import java.sql.SQLException;
import java.util.List;

import com.dog.domain.Crawl;

public interface CrawlDao {

	public void add(Crawl c)throws SQLException;
	
	public void update(Crawl c)throws SQLException;
	
	public void delete(int id)throws SQLException;
	
	public Crawl findById(int id)throws SQLException;
	
	public List<Crawl> findAll()throws SQLException;
	
}
